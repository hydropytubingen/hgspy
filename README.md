# README #

The repository contains two branches so far (maybe more in the future):

      master branch: code that has been tested i.e. working version of the functions
      dev branch   : code still under development and/or on testing step

I would encourage to work directly on <<dev>> branch, at least while we get this thing flying.

For committing by default to dev branch, do the following:
​     
     clone repo:  git clone git@bitbucket.org:hydropytubingen/hgspy.git
     cd \to\repo\directory
     git fetch && git checkout dev

Starting with HGS version 20160929, which is the one available in the HPC-Cluster by the time this repo was created (29.12.2016)
### What is this repository for? ###
Use HydroGeoSphere by using Python, i.e.
- write `*.grok` file
    - write heterogeneous K fields (`idkkk.dat` files)
    - place solute injection locations and durations
    - turn wells on and off
- run `grok`, `phgs`, `hsplot`
- process output
    - BTCs at a well
    - drawdown curves
    - ... 

### Prerequesites
- Python 3.xx
- Numpy xxx
### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact