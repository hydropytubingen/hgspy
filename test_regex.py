#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Hello
"""
import sys
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
import re

# import scipy.optimize as sco
# import scipy.special as ssp 
# import lhglib.contrib.spatial.correlationfunctions as correlationfunctions

__author__ = "Claus Haslauer (mail@planetwater.org)"
__version__ = "$Revision: 1.2 $"
__date__ = datetime.date(2017, 2, 28)
__copyright__ = "Copyright (c) 2017 Claus Haslauer"
__license__ = "Python"


def main():
    plt.ioff()

    f_dir = r'/Users/claushaslauer/Documents/_CodeDev/hgs/hgspy/model_example'
    f_name = 'fl_sim_test1.grok'
    f_obj = os.path.join(f_dir, f_name)

    def add_to_grok(fobj, pattern_looking_for, string_to_add):
        """
        example:
        add another well to existing well, just before `pattern_looking_for`
            pattern_looking_for = "!--- End Well properties"
            string_to_add = "choose segments polyline\n2\n0.000000, 10.000000, 1.000000\n0.000000, 10.000000, 0.000000\nread properties\nTheis\n"

        """
        # open current grok file and read as one string
        with open(f_obj, 'r') as f:
            text = f.read()

        text_to_replace_pattern = string_to_add + pattern_looking_for

        result = re.sub(pattern_looking_for,
                        text_to_replace_pattern,
                        text)
        # write text with replacement in place back to file
        with open(f_obj, 'w') as f:
            f.writelines(result)


    print("Done! Yay!")


if __name__ == '__main__':
    main()