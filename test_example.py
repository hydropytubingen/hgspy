"""
File to run/test the created functions
Created on Wed Mar 25 20:39:31 2015
@author: HydroPy Tubingen
"""
import os
import numpy as np
from hgspy.grokfc import grok as gr, diradmin as da, runmodel as rm


# Create the model folder if does not exists yet
mod_dir = os.path.join(os.curdir, 'model_example')
if not os.path.isdir(mod_dir):
    os.makedirs(mod_dir)

grok_name = 'flow_transient'
mytimes = np.arange(0, 100, 2, dtype=np.float)

# domain geometry
coords = np.array(([0, 10], [0, 10], [0, 2]))
blockgrading = np.array(([0.5, 1, 0.5], [0.5, 1, 0.5], [1, 1, 1]))  # inisize, factor, max size

# output points / observation points
pt_names = ['pt1', 'pt2', 'pt3', 'pt4']
pt_coords = np.array(([2, 2, 1], [4, 2., 1], [4, 4, 1], [6, 6, 1]))
# observation well
well_names = ['w1']
w_coords = np.array(([5, 5, 2, 5, 5, 0]))

# Create first an mprops file, and add two different materials, as an example.
# Use a dictionary to list all properties you want in the props file:
mprops1 = {'k isotropic': 3.0e-4, 'porosity': 0.15}
mprops2 = {'k isotropic': 3.0e-3, 'porosity': 0.30, 'specific storage': 1.2e-7}

mpropsObj = gr.Inputfile(mod_dir, 'exampleprops', 'mprops')
mpropsObj.addMaterial(name='Borden Sand', mode='new', **mprops1)
mpropsObj.addMaterial(name='Borden Gravel', mode='append', **mprops2)

# Create a well props file:
wprops = {'radius': 0.15, 'casing radius': 0.0}
wpropsObj = gr.Inputfile(mod_dir, 'exampleprops', 'wprops')
wpropsObj.addMaterial(name='Theis', mode='new', **wprops)

# Write in disk the grok file, with only the main headers:
grokfile_obj = gr.Grokfile(mod_dir, grokname=grok_name, mkbatch=False, mkletmerun=False)
grokfile_obj.creategrok(description='1d model str example')

# Write in the grid generation part
grokfile_obj.gridgen(gridmethod='grade',
                     coordinates=coords,
                     grade=blockgrading,
                     totecplot=True)


# grokfile_obj.gridgen(gridmethod='meshpy',
#                      zcoordinates_nodal_layers = np.array([10., 5., 0.]),
#                      totecplot=True)

# alternative: uniform
# grokfile_obj.gridgen(gridmethod='uniform',
#                      length=[10, 20, 2],  # if grade, then length of domain in x, y, z
#                      no_elements=[20, 40, 2])  # in x, y, z,

# alternative: uniform with gb input file (top mesh)
# grokfile_obj.gridgen(gridmethod='gb_uniform', mesh_file='gbmeshfile', base=0, top=100, layers=100, totecplot=True)

# Write simulation parameters
grokfile_obj.simulation(units='kms', transient=True, transport=False, cvol=True, fdif=False)

# Create batch files:
grokfile_obj.createprefix()
grokfile_obj.createletmerun(hsplot=True)

# Write media properties section of grok file, may need to agree with the names used to create the mprops file before:
grokfile_obj.mediaProps(mediaType='porous', propsFile='./exampleprops.mprops', mediaName='Borden Sand', typesel='all',
                        component='zone')

grokfile_obj.mediaProps(mediaType='well', propsFile='./exampleprops.wprops', mediaName='Theis', component='segment',
                        typesel='polyline',
                        coords=np.array(([5, 5, 1], [5, 5, 0])), npts=2, kkkfile=None)

grokfile_obj.output_times(times=mytimes)

grokfile_obj.obs_pts(pt_name=pt_names, pt_coord=pt_coords, well_name=well_names,
                     well_coord=w_coords)

grokfile_obj.ini_cond(type_ic='head', val_ini=5.0, mode='flow', domaintype='porous media')

# Test to remove unnecessary files from model directory
files2keep = np.array(['.grok', '.pfx', 'kkkGaus', '.mprops', 'letme', 'wprops', 'iniheads', 'parallelindx'])
fileObj = da.Manipdirs(mod_dir)
filesKept = fileObj.rmfiles(files2keep, mystr=False)

# Well boundary condition:
grokfile_obj.bound_cond(domaintype=None, mode='well',
                        type_bc='flux nodal',
                        component='node',
                        selection=None,
                        coords=np.array([5, 5, 1]),
                        setname='central_pw',
                        bc_time=np.array([0]),
                        bc_value=np.array([10e-3]))

# Head boundary conditions:
grokfile_obj.bound_cond(domaintype='porous media', mode='flow',
                        type_bc='head',
                        component='node',
                        selection='x plane',
                        coords=0,
                        setname='Xo',
                        bc_time=np.array([0]),
                        bc_value=np.array([5]),
                        ptol=1e-7)

# Why not another one :)
grokfile_obj.bound_cond(domaintype=None, mode='flow',
                        type_bc='head',
                        component='node',
                        selection='x plane',
                        coords=10,
                        setname='X1',
                        bc_time=np.array([0]),
                        bc_value=np.array([4.8]),
                        ptol=1e-7)

print('Grok file finished')

# Run Grok and HGS:
rm.fwdHGS(mod_dir, grok_name, hsplot=True)